const { StatusError }= require("../helpers/CustomError");
const errorHandler = require("../helpers/errorHandler");

module.exports = (req, res, next) => {
    const check = new Promise((resolve, reject) => {
        if (!req.decoded.admin) reject(new StatusError("인증 실패", 403));
        resolve();
    });

    const onError = (error) => {
        errorHandler(error, req, res, next);
    };

    check.then(next)
        .catch(onError);
};