const jwt = require("jsonwebtoken");
const { StatusError }= require("../helpers/CustomError");
const errorHandler = require("../helpers/errorHandler");


module.exports = (req, res, next) => {
    const check = new Promise((resolve, reject) => {
        const token = req.headers["x-access-token"] || req.query.token;
        if(!token) reject(new StatusError("인증 실패", 403));
        resolve(token);
    });

    const verify = (token) => {
        return new Promise((resolve, reject) => {
            jwt.verify(token, req.app.get("jwt-secret"), (err, decoded) => {
                if (err) reject(err);
                resolve(decoded);
            });
        });
    };

    const respond = (decoded) => {
        req.decoded = decoded;
        next();
    };

    const onError = (error) => {
        errorHandler(error, req, res, next);
    };

    check
        .then(verify)
        .then(respond)
        .catch(onError);
};