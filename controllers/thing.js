const mongoose = require("mongoose");
const Thing = mongoose.model("Thing");
const errorHandler = require("../helpers/errorHandler");
const { StatusError } = require("../helpers/CustomError");

// POST /thing require admin token
// input: name, token
// output: token, id
module.exports.create = function(req, res, next) {
    let { name, token } = req.body;
    const create = (thing) => {
        if (thing) {
            throw new StatusError("존재하는 사물", 409);
        } else {
            return Thing.create(name, token);
        }
    };

    const respond = (thing) => {
        res.json({
            success: true,
            message: "등록 성공",
            token: thing.token,
            id: thing.id
        });
    };

    const onError = (error) => {
        errorHandler(error, req, res, next);
    };

    Thing.findOne({ name: name })
        .then(create)
        .then(respond)
        .catch(onError);
};

// POST /thing/token require admin token
// input: name
// output: token
module.exports.token = function(req, res, next) {
    let { name } = req.body;

    const respond = (thing) => {
        if (!thing) throw new StatusError("등록된 Thing 이/가 없습니다.", 404);
        res.json({
            success: true,
            message: "토큰",
            token: thing.token,
            id: thing.id
        });
    };

    const onError = (error) => {
        errorHandler(error, req, res, next);
    };

    Thing.findOne({ name: name })
        .then(respond)
        .catch(onError);
};

// // GET /thing/:name require admin token
// // input: name
// // output: id
// module.exports.read = function(req, res, next) {
//     let name = req.params.name;
//
//     const respond = (thing) => {
//         if (!thing) throw new StatusError("등록된 Thing 이/가 없습니다.", 404);
//         res.json({
//             success: true,
//             message: "토큰",
//             id: thing.id
//         });
//     };
//
//     const onError = (error) => {
//         errorHandler(error, req, res, next);
//     };
//
//     Thing.findOne({ name: name })
//         .then(respond)
//         .catch(onError);
// };

// PUT /thing/:id require admin token
// input: id, name, token
// output: token
module.exports.update = function(req, res, next) {
    let id = req.params.id;
    let { token, name } = req.body;

    const updateToken = (thing) => {
        if (!thing) throw new StatusError("등록된 Thing 이/가 없습니다.", 404);
        thing.name = name || thing.name;
        thing.token = token || thing.token;
        return thing.save();
    };

    const respond = (thing) => {
        res.json({
            success: true,
            message: "업데이트 완료",
            token: thing.token
        });
    };

    const onError = (error) => {
        errorHandler(error, req, res, next);
    };

    Thing.findById(id)
        .then(updateToken)
        .then(respond)
        .catch(onError);
};

// DELETE /thing/:id require admin token
// output: true, false
module.exports.delete = function(req, res, next) {
    let id = req.params.id;

    const remove = (thing) => {
        if (thing === null) return Promise.resolve(thing);
        return thing.remove();
    };

    const check = (thing) => {
        if (thing === null) return Promise.resolve(true);
        return new Promise((resolve, reject) => {
            Thing.findById(thing.id, (err, user) => {
                if (err) reject(new StatusError("서버 오류로 삭제 실패", 500));
                if (user === null) resolve(true);
                else reject(new StatusError("삭제 실패", 500));
            });
        });
    };

    const respond = (bool) => {
        res.json({
            success: bool,
            message: "삭제 성공",
        });
    };

    const onError = (error) => {
        errorHandler(error, req, res, next);
    };

    Thing.findById(id)
        .then(remove)
        .then(check)
        .then(respond)
        .catch(onError);
};
