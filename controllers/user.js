const mongoose = require("mongoose");
const User = mongoose.model("User");
const createToken = require("../helpers/createToken");
const errorHandler = require("../helpers/errorHandler");
const { StatusError }= require("../helpers/CustomError");


// POST /user
// input: email, password, provider, thing_id
// output: isAdmin, token
module.exports.create = function(req, res, next) {
    let newUser = null;
    let userToken = null;
    let secret = req.app.get("jwt-secret");
    let { email, password, provider, thing_id } = req.body;

    const create = (user) => {
        if (user) {
            throw new StatusError("존재하는 이메일 주소", 409);
        } else {
            return User.create(email, password, provider, thing_id);
        }
    };

    const token = (user) => {
        newUser = user;
        return createToken(user, secret);
    };

    const count = (token) => {
        userToken = token;
        return User.countDocuments({}).exec();
    };

    const assign = (count) => {
        if (count === 1) return newUser.assignAdmin();
        else return Promise.resolve(false);
    };

    const respond = (isAdmin) => {
        res.json({
            success: true,
            message: "등록 성공",
            admin: !!isAdmin,
            token: userToken
        });
    };

    const onError = (error) => {
        errorHandler(error, req, res, next);
    };

    User.findOneByEmail(email)
        .then(create)
        .then(token)
        .then(count)
        .then(assign)
        .then(respond)
        .catch(onError);
};

// GET /user/ require token
// output: user
module.exports.read = function(req, res, next) {
    const check = new Promise((resolve, reject) => {
        if (req.decoded) resolve(req.decoded);
        else throw new StatusError("서버 오류", 500);
    });

    const respond = (decoded) => {
        res.json({
            success: true,
            message: "사용자 아이디",
            user: {
                id: decoded._id,
                name: decoded.name,
                email: decoded.email,
                isAdmin: decoded.isAdmin
            }
        });
    };

    const onError = (error) => {
        errorHandler(error, req, res, next);
    };

    check
        .then(respond)
        .catch(onError);
};


// POST /user/token
// input: email, password
// output: token
module.exports.token = function(req, res, next) {
    let { email, password, provider } = req.body;
    let secret = req.app.get("jwt-secret");

    const check = new Promise((resolve, reject) => {
        if (email && password) resolve(email);
        else reject(new StatusError("입력 값 확인"), 412);
    });

    const find = (email) => {
        return User.findOneByEmail(email);
    };

    const token = (user) => {
        if (!user) {
            throw new StatusError("존재하지 않는 이메일", 404);
        } else {
            if (user.verify(password)) {
                return createToken(user, secret);
            } else {
                throw new StatusError("인증 실패", 403);
            }
        }
    };

    const respond = (token) => {
        res.json({
            success: true,
            message: "인증 성공",
            token
        });
    };

    const onError = (error) => {
        errorHandler(error, req, res, next);
    };

    check
        .then(find)
        .then(token)
        .then(respond)
        .catch(onError);
};

// PUT / require token
// input: name, password
// output: token
module.exports.update = function(req, res, next) {
    let { name, password } = req.body;
    let secret = req.app.get("jwt-secret");

    const update = (user) => {
        user.name = name;
        user.password = password;
        return user.save();
    };

    const token = (user) => {
        return createToken(user, secret);
    };

    const respond = (token) => {
        res.json({
            success: true,
            message: "변경 성공",
            token
        });
    };

    const onError = (error) => {
        errorHandler(error, req, res, next);
    };

    User.findById(req.decoded._id)
        .then(update)
        .then(token)
        .then(respond)
        .catch(onError);
};

// DELETE / require token
// output: true, false
module.exports.delete = function(req, res, next) {
    const remove = (user) => {
        if (user === null) return Promise.resolve(user);
        return user.remove();
    };

    const check = (user) => {
        if (user === null) return Promise.resolve(true);
        return new Promise((resolve, reject) => {
            User.findById(user.id, (err, user) => {
                if (err) new StatusError("서버 오류로 삭제 실패", 500);
                if (user === null) resolve(true);
                else reject(new StatusError("삭제 실패", 500));
            });
        })
    };

    const respond = (bool) => {
        res.json({
            success: bool,
            message: "삭제 성공",
        });
    };

    const onError = (error) => {
        errorHandler(error, req, res, next);
    };

    User.findById(req.decoded._id)
        .then(remove)
        .then(check)
        .then(respond)
        .catch(onError);
};