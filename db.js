const mongoose = require("mongoose");
const config = require("./config");

let dbURI = config.mongodbURI;
mongoose.connect(dbURI, { useNewUrlParser: true });

mongoose.connection.on("connected", () => {
    console.log("Mongoose connected to " + dbURI);
});
mongoose.connection.on("error", (error) => {
    console.error("Mongoose connection error: " + error);
});
mongoose.connection.on("disconnected", () => {
    console.log("Mongoose disconnected");
});

process.on("SIGINT", () => {
    gracefulShutdown("app termination", () => { process.exit(0) });
});
function gracefulShutdown (msg, callback) {
    mongoose.connection.close(() => {
        console.log("Mongoose disconnected through " + msg);
        callback();
    });
}

require("./models/User");
require("./models/Thing");