const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const config = require("./config");

let userRouter = require("./routes/user");
let thingRouter = require("./routes/thing");

let app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.set("jwt-secret", config.secret);
// app.use(express.static(path.join(__dirname, "public")));

app.use("/user", userRouter);
app.use("/thing", thingRouter);

module.exports = app;
