const jwt = require("jsonwebtoken");


module.exports = (user, secret) => {
    return new Promise((resolve, reject) => {
        jwt.sign(
            {
                _id: user._id,
                email: user.email,
                name: user.name,
                admin: user.admin
            },
            secret,
            {
                expiresIn: "7d",
                issuer: "mital",
                subject: "userInfo"
            }, (err, token) => {
                if (err) reject(err);
                resolve(token);
            });
    });
};