const mongoose = require("mongoose");
const Schema = mongoose.Schema;


let User = new Schema({
    name: String,
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    admin: {
        type: Boolean,
        "default": false
    },
    provider: { type: String, "default": "local" },
    things: [{ type: Schema.Types.ObjectId, ref: "Thing" }]
});

// todo 비밀번호 저장 전 암호화 해야함
User.statics.create = function (email, password, provider, thing_id) {
    let user = new this({ email: email, password: password, provider: provider });
    user.things.push(thing_id);
    return user.save();
};

User.statics.findOneByEmail = function (email) {
    return this.findOne({ email: email }).exec();
};

// todo 암호화 해야함
User.methods.verify = function(password) {
    return this.password === password
};

// 관리자 계정으로 설정
User.methods.assignAdmin = function() {
    this.admin = true;
    return this.save();
};


mongoose.model("User", User);