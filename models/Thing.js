const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let Thing = new Schema({
    name: { type: String, required: true },
    token: { type: String, required: true }
});

Thing.statics.create = function (name, token) {
    let thing = new this({ name: name, token: token });
    return thing.save();
};

mongoose.model("Thing", Thing);