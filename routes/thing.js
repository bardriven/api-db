const express = require("express");
const router = express.Router();
const controller = require("../controllers/thing");
const auth = require("../middlewares/auth");
const authAdmin = require("../middlewares/authAdmin");

router.use("/", auth, authAdmin);
router.post("/", controller.create);
router.post("/token", controller.token);
// router.get("/:name", controller.read);
router.put("/:id", controller.update);
router.delete("/:id", controller.delete);

module.exports = router;
