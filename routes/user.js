const express = require("express");
const router = express.Router();
const controller = require("../controllers/user");
const auth = require("../middlewares/auth");


router.post("/", controller.create);
router.post("/token", controller.token);
router.use("/", auth);
router.get("/", controller.read);
router.put("/", controller.update);
router.delete("/", controller.delete);

module.exports = router;
